# README

<https://www.codecademy.com/article/building-a-todolist-with-rails>

- Ruby version: 3.3.0

- Start database:

```bash
docker-compose up
```

- Aplicar migrações:

```bash
rails db:migrate
```

- Install dependencies:

```bash
bundle install
```

- Compilar assets

```bash
rails assets:precompile
```

- Running:

```bash
./bin/rails server
```

- Tailwind:

```bash
rails tailwindcss:watch
```

- Server + Tailwind:

```bash
./scripts/run.sh
```

- Clean Cache

```bash
./scripts/cache-clear.sh
```

- URL:

<http://localhost:3000/>

- Testes

<https://guides.rubyonrails.org/testing.html>
