class TodosController < ApplicationController
  def index
    @todos = Todo.all
  end

  def create
    @todo = Todo.create(description: params[:todo][:description])
    return unless @todo.valid?

    redirect_to todos_path
  end

  def destroy
    @todo = Todo.find(params[:id])
    return unless @todo.destroy

    redirect_to todos_path
  end
end
